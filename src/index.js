// Import the react and reactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';

// function getButtonText() {
//   return 'Click on me!';
// }

function getCurrentTime() {
  return (new Date().toLocaleTimeString());
}

// Create a react component
const App = () => {
  const buttonText = 'Click Me!';
  // const buttonText = 1230;
  // const buttonText = [12, 10];
  // const buttonText = {text: 'Click Me'};
  const labelText = 'Enter name:';
  // const style = { backgroundColor: 'blue', color: 'white' };

  return (
    <div>
      <label htmlFor="name" className="label">
        {labelText}
      </label>
      <input type="text" id="name" />
      <button style={{ backgroundColor: 'blue', color: 'white' }}>
        {buttonText}
        {/* {buttonText.text} */}
        {/* {getButtonText()} */}
      </button>
      <p>
        Current Time: {getCurrentTime()}
      </p>
    </div>
  );
}

// Take the react component and show it on the screen
ReactDOM.render( <App />, document.querySelector('#root'));
